#!/usr/bin/env ruby
require 'openssl'
require 'benchmark'

bits = (ARGV[0] || 50).to_i

# 2-bits p are boring...
fail 'Can only handle 3-63 bits integer' unless bits >= 3 && bits <= 63

p = 0

loop do
  p = rand((1<<(bits-1))...(1<<bits))
  break if p.to_bn.prime?
end

a = rand(2...p-1)
x = rand(2...p-1)
b = a.to_bn.mod_exp(x, p).to_i

puts "Generated problem: #{a}^x = #{b} (mod #{p})"
puts "Answer: x = #{x}"

ret1 = nil

puts
puts '=== Running Pollard-rho method ==='
time = Benchmark.measure do
  ret1 = `./rho #{a} #{b} #{p}`
end
puts time

if ret1[/found ans = (\d+)/]
  sol = $1.to_i
  puts "Pollard-rho found solution: #{sol}"
  fail 'Pollard-rho found incorrect solution!' unless a.to_bn.mod_exp(sol, p) == b
else
  fail 'Pollard-rho can\'t find a solution!'
end
