#pragma once
#include <bits/stdc++.h>
#include <gmp.h>
using namespace std;

typedef int64_t i64;

default_random_engine generator(random_device{}());

int prime_test(i64 n) {
  mpz_t mpzn;
  mpz_init(mpzn);
  mpz_set_ui(mpzn, n);
  int ret = mpz_probab_prime_p(mpzn, 25);
  mpz_clear(mpzn);
  return ret;
}

inline i64 mod_add(i64 a, i64 b, i64 n) {
  a += b;
  if (a >= n) a -= n;
  return a;
}

inline i64 mod_mul(i64 a, i64 b, i64 n) {
  return (__uint128_t)a * b % n;
}

inline i64 mod_exp(i64 a, i64 b, i64 n) {
  a %= n;
  i64 ret = 1;
  while (b) {
    if (b & 1) {
      ret = mod_mul(ret, a, n);
    }
    a = mod_mul(a, a, n);
    b >>= 1;
  }
  return ret;
}

inline i64 mod(i64 a, i64 n) {
  a %= n;
  return a < 0 ? a + n : a;
}

inline i64 mod_inverse(i64 a, i64 b) {
  if (a == 1) return 1;
  i64 ret = mod_inverse(b % a, a);
  return mod((1 - (__int128_t)ret * b) / a, b);
}

i64 a, b, p, n;

inline void init(int argc, char* argv[]) {
  if (argc != 4) {
    fprintf(stderr, "Usage: %s [alpha] [beta] [p]\n", argv[0]);
    fprintf(stderr, "  Solve [alpha]^x = [beta] (mod [p])\n");
    exit(1);
  }
  a = strtoull(argv[1], NULL, 10);
  b = strtoull(argv[2], NULL, 10);
  p = strtoull(argv[3], NULL, 10);
  a = mod(a, p); b = mod(b, p);
  if (prime_test(p) == 0) {
    fprintf(stderr, "p must be a prime!\n");
    exit(1);
  }
  n = p - 1; // We need p to be prime, or we don't know how to calculate n=phi(p)...
}
