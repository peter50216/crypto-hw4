CXXFLAGS=-Ofast -std=c++11 -march=native -pipe
LDFLAGS+=-lgmp

all: rho

clean:
	$(RM) rho
