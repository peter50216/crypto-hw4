#include "dlp.h"
i64 p1, p2;

inline int h(i64 x) {
  if (x < p1) return 0;
  else if (x < p2) return 1;
  else return 2;
}

int main(int argc, char* argv[]) {
  init(argc, argv);
  p1 = p / 3;
  p2 = p - p / 3;
  uniform_int_distribution<i64> distribution(0, n-1);
  auto rand_val = bind(distribution, generator);
  i64 a1 = rand_val() % n;
  i64 b1 = rand_val() % n;
  i64 beta1 = mod_mul(mod_exp(a, a1, p), mod_exp(b, b1, p), p);
  i64 i = 1;
  i64 ans;
  bool found = false;
  while (i < 0x10000000000L) {
    i64 ai = a1, bi = b1, beta = beta1;
    for (i64 j = i + 1; j <= 2 * i; j++) {
      int k = h(beta);
      switch(k) {
        case 0:
          ai = mod_add(ai, 1, n);
          beta = mod_mul(beta, a, p);
          break;
        case 1:
          ai = mod_add(ai, ai, n);
          bi = mod_add(bi, bi, n);
          beta = mod_mul(beta, beta, p);
          break;
        case 2:
          bi = mod_add(bi, 1, n);
          beta = mod_mul(beta, b, p);
          break;
        default:
          assert(false);
      }
      if (beta == beta1 && b1 != bi) {
        i64 ad = mod(a1 - ai, n);
        i64 bd = mod(bi - b1, n);
        i64 g = __gcd(n, bd);
        if (g <= 100000 && ad % g == 0) { // Just try them all!
          i64 tn = n / g;
          ad /= g; bd /= g;
          i64 tmp = mod_mul(ad, mod_inverse(bd, tn), tn);
          for (int i = 0; i < g; i++) {
            if (mod_exp(a, tmp, p) == b) {
              ans = tmp;
              found = true;
              goto out;
            }
            tmp += tn;
          }
        }
      }
    }
    a1 = ai; b1 = bi; beta1 = beta; i *= 2;
  }
out:;
    if (found) {
      printf("found ans = %ld\n", ans);
    } else {
      printf("ans not found.\n");
    }
}
